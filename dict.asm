%include "lib.inc"
global find_word

find_word:
.while:
cmp rsi, 0
je .false
push rsi
push rdi
add rsi, 8
mov rsi, [rsi]
call string_equals
pop rdi
pop rsi
cmp rax, 1
je .true
mov rsi, [rsi]
jmp .while
.true:
mov rax, rsi
ret
.false:
mov rax, 0
ret
