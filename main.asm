section .data
bad_read: db "Не удалось считать строку",10,0
no_word: db "Слово не найдено",10,0

%include "lib.inc"
%include "words.inc"
section .text
global _start
extern find_word

%define BUFFER_SIZE 256

_start:
    mov rcx, rsp
    mov rsi, rsp
    sub rsp, BUFFER_SIZE
    mov rdx, BUFFER_SIZE
    push rcx
    call read_string
    pop rcx
    cmp rax, 0
    je .read_er
    mov rdi, rcx
    mov rsi, HEAD
    push rcx
    call find_word
    pop rcx
    cmp rax, 0
    je .no_word
    add rax, 16
    mov rdi, [rax]
    push rcx
    call print_string
    pop rcx
    mov rdi, 0
    jmp .exit

.read_er:
    mov rdi, bad_read
    push rcx
    call print_err
    pop rcx
    mov rdi, 5
    jmp .exit

.no_word:
    mov rdi, no_word
    push rcx
    call print_err
    pop rcx
    mov rdi, 22
    jmp .exit

.exit:
    mov rsp, rcx
    call exit
 
