%define HEAD 0
%macro colon 2
%%x:
dq HEAD
%define HEAD %%x
dq %%key
dq %2

%%key: db %1, 0
%2:
%endmacro
