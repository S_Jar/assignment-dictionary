ASM=nasm
ASMFLAGS=-f elf64
LD=ld

all: program

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm words.inc colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

program: main.o dict.o lib.o
	$(LD) -o $@ $^

clean:
	rm *.o
